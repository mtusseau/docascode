package org.docascode.cli;

import picocli.CommandLine;

public class Main{
	public static void main(String[] args) {
		CommandLine.run(new DocAsCode(), args);
	}
}