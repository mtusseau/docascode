package org.docascode.cli;

import org.docascode.api.DiffCommand;
import org.docascode.api.DocAsCode;
import org.docascode.api.core.errors.DocAsCodeException;
import picocli.CommandLine;

import java.io.File;

@CommandLine.Command(name = "diff",
    description = "Compute and display a comparison of files between either Git revision or Maven versions.")
public class Diff extends Command implements Runnable {

    @CommandLine.Option(names = {"--file","-f"}, arity="1", description = "The file to compare")
    private File file;

    @CommandLine.Option(names = {"--base","-b"}, arity="1", description = "The base revision or version. (default: ${DEFAULT-VALUE}).")
    private String base = "LATEST";

    @CommandLine.Option(names = {"--revised","-r"}, arity="1", description = "the new revision or version. (default: ${DEFAULT-VALUE}).")
    private String revised = "WorkTree";

    @Override
    public void run() {
        try (DocAsCode docascode = DocAsCode.open()){
            DiffCommand diff = ((DiffCommand )docascode
                    .diff()
                    .addListener(this))
                    .setOldRevision(base)
                    .setNewRevision(revised);
            File toFile = new File(
                        docascode.getRepository().getTmpDir(),
                        docascode.getRepository().relativize(file));
                diff.setFile(file)
                        .toFile(toFile)
                        .openAtEnd(true)
                        .call();
        } catch (DocAsCodeException e) {
            error(e);
        }
    }
}
