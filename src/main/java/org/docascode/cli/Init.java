package org.docascode.cli;

import org.docascode.api.InitCommand;
import picocli.CommandLine;
import java.io.File;
import org.docascode.api.DocAsCode;
import org.docascode.api.core.errors.DocAsCodeException;

@CommandLine.Command(name = "init",
    description = "Initialize or upgrade a DocAsCode repository from either a directory, a Git repository or a DocAsCode Repository.")
public class Init extends Command implements Runnable {
    @CommandLine.Parameters(arity = "0..1", description = "The name of directory to initialize")
    private File directory = new File(".");

    @Override
    public void run() {
        try {
            ((InitCommand)DocAsCode.init().addListener(this))
                    .setDirectory(directory).call();
        } catch (DocAsCodeException e) {
            error(e);
        }
    }
}
