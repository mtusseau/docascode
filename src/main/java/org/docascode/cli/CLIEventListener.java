package org.docascode.cli;

import org.apache.log4j.Logger;
import org.docascode.api.DocAsCode;
import org.docascode.api.core.errors.DocAsCodeException;
import org.docascode.api.event.Event;
import org.docascode.api.event.ProgressEvent;
import org.docascode.api.listener.EventListener;

public class CLIEventListener implements EventListener {
    private static Logger logger = Logger.getLogger(CLIEventListener.class);

    @Override
    public void fireEvent(Event event) {
        String msg;
        if (event instanceof ProgressEvent){
            ProgressEvent p = (ProgressEvent) event;
            msg = String.format("%s: %3d%% (%s/%s)...\r",
                    p.getMessage(),
                    100*p.getProgress()/p.getTotal(),
                    p.getProgress(),
                    p.getTotal());
        } else {
            msg = String.format("%s%n",
                    event.getMessage());
        }
        switch (event.getLevel()){
            case SUCESS:
                success(msg);
                break;
            case INFO:
                info(msg);
                break;
            case WARN:
                warn(msg);
                break;
            case DEBUG:
                debug(msg);
                break;
            default:
                break;
        }
    }

    void success(String msg){
        logger.info(msg);
    }

    protected void info(String msg){
        logger.info(msg);
    }

    protected void warn(String msg){
        logger.warn(msg);
    }

    protected void debug(String msg) {
        logger.debug(msg);
    }

    protected void error(String msg) {
        logger.error(String.format("Error: %s",
                msg));
    }

    protected void error(Exception e){
        error(String.format("Error. %s%n",
                    e.getMessage()));
        try {
            logger.error(String.format(
                    "Build Number: '%s'%n",
                    DocAsCode.getBuildNumber()));
        } catch (DocAsCodeException er) {
            logger.error(er);
        }
        logger.error("Stacktrace",e);
        logger.error("You can submit an issue at 'https://gitlab.com/docascode/docascode/issues'.");
        logger.error(String.format(
                "Please give the logs given in %s/.docascode/docascode.log.%n",System.getProperty("user.home")));
        System.exit(1);
    }
}
