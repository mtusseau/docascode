package org.docascode.cli;

import picocli.CommandLine;

public class Command extends CLIEventListener {
    @CommandLine.Option(names = {"-h", "--help"}, usageHelp = true, description = "display this help message")
    boolean usageHelpRequested;
}
