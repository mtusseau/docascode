package org.docascode.cli;

import org.docascode.api.core.errors.DocAsCodeException;
import picocli.CommandLine;

import java.util.List;

@CommandLine.Command(name = "docascode",
        versionProvider = DocAsCode.ManifestVersionProvider.class,
        subcommands = {
            Update.class,
            Init.class,
            Add.class,
            Build.class,
            Remove.class,
            PreCommit.class,
            PostCommit.class,
            Diff.class,
            Merge.class},
        description="DocAsCode is a toolchain designed for handling and delivering OpenXML files. See more at 'https://gitlab.com/docascode/docascode'.")
public class DocAsCode extends Command implements Runnable{

    static class ManifestVersionProvider implements picocli.CommandLine.IVersionProvider {
        public String[] getVersion() throws DocAsCodeException {
            List<String> versionInfos = org.docascode.api.DocAsCode.getVersionInformations();
            String[] versions = new String[versionInfos.size()];
            versions = versionInfos.toArray(versions);
            return versions;
            }
        }

    @CommandLine.Option(names = { "-v", "--version" }, versionHelp = true, description = "Show version information and exit")
    boolean versionHelpRequested;

    @Override
    public void run() {
        usageHelpRequested=true;
    }
}