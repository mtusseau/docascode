package org.docascode.api.diff;

import org.apache.commons.io.FilenameUtils;
import org.docascode.api.core.errors.DocAsCodeException;
import org.docascode.api.event.Event;

import java.awt.*;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Differencer extends AbstractDifferencer{
    @Override
    public void diff() throws DocAsCodeException {
        AbstractDifferencer diff;
        String extension = FilenameUtils.getExtension(targetFile.getAbsolutePath());
        if (extension.equals("docx")) {
            diff = new DocxDifferencer();
        } else {
            log(String.format(
                    "No Diff driver for %s.",
                    targetFile.getAbsolutePath()), Event.Level.INFO);
            try {
                Files.delete(Paths.get(targetFile.getAbsolutePath()));
            } catch (IOException e) {
                log(String.format("Unable to delete '%s'. Please do it manually.",targetFile.getAbsolutePath()), Event.Level.WARN);
                log(String.format("Cause: %s",e.getMessage()), Event.Level.DEBUG);
            }
            return;
        }
        ((AbstractDifferencer) diff.addListener(this))
             .setBaseFile(this.baseFile)
             .setRevisedFile(this.revisedFile)
             .setTargetFile(this.targetFile)
             .setRevision(this.revision)
             .openAtEnd(openAtEnd)
             .diff();
        if (openAtEnd) {
            Desktop desktop;
            if (Desktop.isDesktopSupported()) {
                desktop = Desktop.getDesktop();
                try {
                    Event e = new Event(this);
                    e.setMessage(
                            String.format(
                                    "Opening '%s'.",
                                    targetFile));
                    e.setLevel(Event.Level.SUCESS);
                    fireEvent(e);
                    desktop.open(targetFile);
                } catch (IOException e) {
                    throw new DocAsCodeException(
                            String.format(
                                    "Unable to open file '%s",
                                    targetFile), e);
                }
            }
        }
    }
}
