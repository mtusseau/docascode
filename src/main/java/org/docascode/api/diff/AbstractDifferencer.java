package org.docascode.api.diff;

import org.docascode.api.core.errors.DocAsCodeException;
import org.docascode.api.listener.APIEventListener;

import java.io.File;

public class AbstractDifferencer extends APIEventListener {
    File baseFile;

    public AbstractDifferencer setBaseFile(File baseFile){
        this.baseFile = baseFile;
        return this;
    }

    File revisedFile;

    public AbstractDifferencer setRevisedFile(File revisedFile){
        this.revisedFile = revisedFile;
        return this;
    }

    protected String revision;

    public AbstractDifferencer setRevision(String revision){
        this.revision = revision;
        return this;
    }

    File targetFile;

    public AbstractDifferencer setTargetFile(File targetFile){
        this.targetFile = targetFile;
        return this;
    }

    public void diff() throws DocAsCodeException {
        throw new UnsupportedOperationException();
    }

    boolean openAtEnd = false;

    public AbstractDifferencer openAtEnd(boolean openAtEnd){
        this.openAtEnd = openAtEnd;
        return this;
    }
}
