package org.docascode.api.listener;

import org.docascode.api.event.Event;
import org.docascode.api.event.ProgressEvent;

import java.util.ArrayList;
import java.util.List;

public class APIEventListener implements EventListener {

    private List<EventListener> listeners = new ArrayList<>();

    public APIEventListener addListener(EventListener listener){
        if (!this.listeners.contains(listener)) {
            this.listeners.add(listener);
        }
        return this;
    }

    public APIEventListener removeListener(EventListener listener){
        if (this.listeners.contains(listener)) {
            this.listeners.remove(listener);
        }
        return this;
    }


    @Override
    public void fireEvent(Event e){
        for (EventListener l : this.listeners){
            l.fireEvent(e);
        }
    }

    protected void log(String message, Event.Level level){
        Event e = new Event(this);
        e.setMessage(message)
                .setLevel(level);
        fireEvent(e);
    }

    protected void progress(int progress, int total, String message, Event.Level level){
        ProgressEvent e = new ProgressEvent(this);
        ((ProgressEvent) e.setMessage(message)
                .setLevel(level))
            .setProgress(progress)
            .setTotal(total);
        fireEvent(e);
    }
}
