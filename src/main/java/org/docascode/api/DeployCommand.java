package org.docascode.api;

import org.docascode.api.core.DocAsCodeRepository;
import org.docascode.api.core.errors.DocAsCodeException;
import org.docascode.api.core.mvn.Artifacts;

import java.util.ArrayList;
import java.util.List;

public class DeployCommand extends DocAsCodeCommand<DeployCommand> {
    private List<Artifacts> artifacts = new ArrayList<>();

    private String profile;

    DeployCommand(DocAsCodeRepository repo) {
        super(repo);
    }

    public DeployCommand addArtifacts(List<Artifacts> artifacts) {
        this.artifacts.addAll(artifacts);
        return this;
    }

    public DeployCommand addIds(List<String> ids) throws DocAsCodeException {
        this.artifacts.addAll(
                getRepository().chrono().toArtifacts(ids).toList());
        return this;
    }

    @Override
    public DeployCommand call() throws DocAsCodeException {
        ((DocAsCodeRepository) getRepository().addListener(this))
                .mvn()
                .addRemoteRepositories(this.profile)
                .deploy(this.artifacts);
        getRepository().removeListener(this);
        return this;
    }

    public DeployCommand setProfile(String profile) {
        this.profile = profile;
        return this;
    }
}
