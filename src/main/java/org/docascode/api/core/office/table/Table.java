package org.docascode.api.core.office.table;

import org.docascode.api.core.office.ParagraphFormat;

import java.util.ArrayList;
import java.util.List;

public class Table {
    private int num = -1;

    private List<Row> rows = new ArrayList<>();

    private ParagraphFormat paragraphFormat;

    public Table setNum(int num){
        this.num = num;
        return this;
    }

    int getNum(){
        return num;
    }


    public Table add(Row row){
        this.rows.add(row);
        return this;
    }

    List<Row> getRows(){
        return this.rows;
    }

    public Table setParagraphFormat(ParagraphFormat paragraphFormat) {
        this.paragraphFormat = paragraphFormat;
        return this;
    }

    ParagraphFormat getParagraphFormat(){
        return this.paragraphFormat;
    }



}
