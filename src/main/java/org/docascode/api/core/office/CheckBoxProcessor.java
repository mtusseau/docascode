package org.docascode.api.core.office;

import org.apache.commons.io.FilenameUtils;
import org.docascode.api.core.errors.DocAsCodeException;
import org.docascode.api.event.Event;
import org.docascode.api.listener.APIEventListener;
import org.docx4j.jaxb.Context;
import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.docx4j.wml.*;

import javax.xml.bind.JAXBElement;
import java.io.File;
import java.util.List;

public class CheckBoxProcessor extends APIEventListener {
    public void setStatus(File file, String name, Boolean checked) throws DocAsCodeException {
        try {
            String extension = FilenameUtils.getExtension(file.getAbsolutePath());
            WordprocessingMLPackage openPackage;
            switch (extension) {
                case "docx":
                case "docm":
                    openPackage = WordprocessingMLPackage.load(file);
                    break;
                default:
                    log(String.format(
                            "Unsupported file extension '%s'",file.getAbsolutePath()
                    ), Event.Level.WARN);
                    return;
            }
            CTFFCheckBox checkbox = findCheckBox(openPackage,name);
            if (checkbox != null){
                org.docx4j.wml.ObjectFactory factory = Context.getWmlObjectFactory();
                BooleanDefaultTrue b = factory.createBooleanDefaultTrue();
                b.setVal(checked);
                checkbox.setChecked(b);
            } else {
                throw new DocAsCodeException(String.format(
                        "Unable to find checkbox '%s' in file '%s'",
                        name,
                        file.getAbsolutePath()));
            }
            openPackage.save(file);
        } catch (
                Docx4JException e) {
            throw new DocAsCodeException(
                    String.format("Unable to set status '%s' to checkbox '%s' in file %s",
                            checked,
                            name,
                            file.getAbsolutePath())
                    ,e);

        }
    }

    public static CTFFCheckBox findCheckBox (WordprocessingMLPackage pkg, String name){
        List<FldChar> listFieldChar = Utils.getTargetElements(pkg.getMainDocumentPart(), FldChar.class);
        boolean found = false;
        CTFFCheckBox checkbox = null;
        int i = 0;
        while (!found && i < listFieldChar.size()) {
            CTFFData ctffData = listFieldChar.get(i).getFfData();
            if (ctffData != null) {
                for (JAXBElement e : ctffData.getNameOrEnabledOrCalcOnExit()) {
                    if (e.getDeclaredType() == CTFFName.class) {
                        found = (((CTFFName) e.getValue()).getVal().equals(name));
                    }
                    if (e.getDeclaredType() == CTFFCheckBox.class) {
                        checkbox = (CTFFCheckBox) e.getValue();
                    }
                }
            }
            i++;
        }
        return found ? checkbox : null;
    }
}
