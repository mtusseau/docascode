package org.docascode.api.core;

import org.docascode.api.core.chrono.ChronoRepository;
import org.docascode.api.core.errors.DocAsCodeException;
import org.docascode.api.core.git.GitRepository;
import org.docascode.api.core.mvn.MvnRepository;
import org.docascode.api.listener.APIEventListener;
import org.eclipse.jgit.errors.ConfigInvalidException;
import org.eclipse.jgit.lib.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class DocAsCodeRepository extends APIEventListener {
    private File docascodeDir;

    private GitRepository gitRepository;

    /**
     * A DocAsCodeRepository constructor
     * @param repo The base Git Repository.
     */

    public DocAsCodeRepository(Repository repo) throws IOException {
        gitRepository = new GitRepository(repo);
        docascodeDir = new File(getWorkTree(),".docascode");
    }

    /**
     * The workTree of the current DocAsCode/Git Repository.
     */

    public File getWorkTree(){
        return gitRepository.getWorkTree();
    }

    /**
     * The Maven project settings file for this DocAsCode repository.
     * @return the Maven project file.
     */

    private File getProjectSettingsFile() {
        return new File(getDocAsCodeDir(),"settings.xml");
    }


    private MvnRepository mvnRepository;

    /**
     * A Maven Repository Controller for this DocAsCode repository.
     * @return the MvnRepository
     */

    public MvnRepository mvn(){
        if (mvnRepository==null){
            mvnRepository = new MvnRepository(getProjectSettingsFile(),getWorkTree());
            mvnRepository.addListener(this);
        }
        return mvnRepository;
    }

    private ChronoRepository chronoRepository;

    /**
     * A Chrono Repository Controller for this DocAsCode repository.
     * @return the ChronoRepository
     */

    public ChronoRepository chrono(){
        if (chronoRepository==null){
            chronoRepository = new ChronoRepository(getChronoXML(),getWorkTree());
            chronoRepository.addListener(this);
        }
        return chronoRepository;
    }

    /**
     * The delivery.properties file for this repository.
     * @return the delivery.properties file.
     */

    public File getDeliveryProperties() {
        return new File(getWorkTree(),"delivery.properties");
    }

    /**
     * The .docascode directory.
     */

    public File getDocAsCodeDir(){
        return this.docascodeDir;
    }

    /**
     * The chrono.xml file.
     */

    public File getChronoXML(){
        return new File(getDocAsCodeDir(),"chrono.xml");
    }

    public File  getDeliveryXML(){
        return new File(getWorkTree(),"delivery.xml");
    }

    /**
     * Resolve file path relatively to the worktree of this DocAsCode repository.
     * Paths are returned in Unix styles.
     * @param file the file to resolve.
     * @return the resolved path.
     */

    public String relativize (File file){
        Path workTreePath = Paths.get(getWorkTree().getAbsolutePath());
        Path filePath = Paths.get(file.getAbsolutePath());
        return workTreePath.relativize(filePath).normalize().toString().replace("\\","/").replace("../","");
    }

    /**
     * The temporary working directory for this DocAsCode repository.
     * @return the temporary working directory.
     */

    public File getTmpDir(){
        return new File(getDocAsCodeDir(),"tmp");
    }

    /**
     * The version of this DocAsCode repository.
     * Usually this version is set through an init or upgrade
     * @return this DocAsCode repository version
     */

    public String getVersion() throws DocAsCodeException {
        StoredConfig c = git().getProjectConfig();
        try {
            c.load();
            return c.getString("docascode",null,"version");
        } catch (IOException | ConfigInvalidException e) {
            throw new DocAsCodeException("Unable to get current repository version",e);
        }
    }

    /**
     * A Git Repository Controller for this DocAsCode repository.
     * @return the GitRepository.
     */

    public GitRepository git() {
        return gitRepository;
    }
}
