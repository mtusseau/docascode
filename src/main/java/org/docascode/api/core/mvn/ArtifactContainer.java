package org.docascode.api.core.mvn;

import org.eclipse.aether.artifact.Artifact;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ArtifactContainer {
    private Map<String,Artifacts> artifacts = new HashMap<>();

    public ArtifactContainer add(Artifact artifact){
        String gav = String.format("%s:%s:%s",
                artifact.getGroupId(),
                artifact.getArtifactId(),
                artifact.getVersion());
        if (!artifacts.containsKey(gav)){
            artifacts.put(gav,new Artifacts());
        }
        artifacts.get(gav).add(artifact);
        return this;
    }

    public List<Artifacts> toList(){
        return new ArrayList<>(artifacts.values());
    }
}
