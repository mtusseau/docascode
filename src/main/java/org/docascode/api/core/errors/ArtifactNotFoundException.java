package org.docascode.api.core.errors;

public class ArtifactNotFoundException extends DocAsCodeException {
    public ArtifactNotFoundException(String message) {
        super(message);
    }
}
