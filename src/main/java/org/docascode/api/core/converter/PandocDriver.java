package org.docascode.api.core.converter;

import org.apache.commons.io.FilenameUtils;
import org.docascode.api.core.errors.ProcessingException;
import java.io.IOException;


public class PandocDriver extends AbstractConverter{
    private String pandocExec = "pandoc";

    @Override
    public void convert() throws ProcessingException {
        try {
            toFile.getParentFile().mkdirs();
            ProcessBuilder builder = new ProcessBuilder(
                    String.format("%s",getPandocExec()),
                    String.format("--extract-media=%s",FilenameUtils.getBaseName(toFile.getAbsolutePath())),
                    String.format("%s",fromFile.getAbsolutePath()),
                    "--wrap=none",
                    String.format("--output=%s",toFile.getAbsolutePath()));
            builder.directory(toFile.getParentFile());
            builder.redirectErrorStream(true);
            Process process = builder.start();
            process.waitFor();
        } catch (IOException e) {
            throw new ProcessingException("Unable to execute pandoc driver",e);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    private String getPandocExec() {
        return pandocExec;
    }
}
