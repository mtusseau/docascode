package org.docascode.api.core.converter;

import com.documents4j.api.DocumentType;
import com.documents4j.api.IConverter;
import com.documents4j.job.LocalConverter;
import org.docascode.api.core.errors.ProcessingException;


import java.io.*;

public class DocxToPdfDriver extends AbstractConverter{
    @Override
    public void convert() throws ProcessingException {
        if (!toFile.getParentFile().exists() && !toFile.getParentFile().mkdirs()){
            throw new ProcessingException(
                String.format("Unable to create directory '%s'.",toFile.getParentFile()));
        }
        try (OutputStream os = new FileOutputStream(toFile);
            InputStream is = new FileInputStream(fromFile)){
            IConverter converter = LocalConverter.builder().build();
            converter.convert(is).as(DocumentType.DOCX)
                    .to(os)
                    .as(DocumentType.PDF).execute();
        } catch (IOException e) {
            throw new ProcessingException(
                    String.format(
                            "Unable to convert '%s' to '%s",
                            fromFile,
                            toFile),e);
        }
    }
}
