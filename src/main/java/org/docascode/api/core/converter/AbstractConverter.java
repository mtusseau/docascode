package org.docascode.api.core.converter;

import org.docascode.api.core.errors.ProcessingException;
import org.docascode.api.listener.APIEventListener;

import java.io.File;

public class AbstractConverter extends APIEventListener {
    protected File fromFile;
    protected File toFile;

    public AbstractConverter fromFile(File fromFile) {
        this.fromFile=fromFile;
        return this;
    }

    public AbstractConverter toFile(File toFile) {
        this.toFile=toFile;
        return this;
    }

    public void convert() throws ProcessingException {
        throw new UnsupportedOperationException();
    }
}
