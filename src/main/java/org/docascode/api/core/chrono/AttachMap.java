package org.docascode.api.core.chrono;

import org.docascode.api.core.chrono.generated.BaseArtifact;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.TreeMap;

@XmlJavaTypeAdapter(AttachMapAdapter.class)
public class AttachMap extends TreeMap<String, BaseArtifact> {

}
