package org.docascode.api.core.chrono;

import org.docascode.api.core.chrono.generated.AttachedArtifacts;
import org.docascode.api.core.chrono.generated.BaseArtifact;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.util.Map;


public class AttachMapAdapter extends XmlAdapter<AttachedArtifacts, AttachMap> {
    @Override
    public AttachMap unmarshal(AttachedArtifacts attachArtifact){
        AttachMap map = new AttachMap();
        for (AttachedArtifacts.Attach e : attachArtifact.getAttach())
        {
            BaseArtifact b = new BaseArtifact();
            b.setOutputs(e.getOutputs());
            b.setFile(e.getFile());
            map.put(e.getClassifier(), b);
        }
        return map;
    }

    @Override
    public AttachedArtifacts marshal(AttachMap map) {
        if (map == null || map.entrySet().isEmpty()) {
            return null;
        } else {
            AttachedArtifacts modeller = new AttachedArtifacts();
            for (Map.Entry<String, BaseArtifact> entry : map.entrySet()) {
                AttachedArtifacts.Attach e = new AttachedArtifacts.Attach();
                e.setClassifier(entry.getKey());
                e.setFile(entry.getValue().getFile());
                e.setOutputs(entry.getValue().getOutputs());
                modeller.getAttach().add(e);
            }
            return modeller;
        }
    }
}
