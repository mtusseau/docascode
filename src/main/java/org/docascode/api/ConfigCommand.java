package org.docascode.api;

import org.docascode.api.core.errors.DocAsCodeException;
import org.docascode.api.core.DocAsCodeRepository;
import org.eclipse.jgit.errors.ConfigInvalidException;
import org.eclipse.jgit.lib.StoredConfig;
import org.eclipse.jgit.util.FS;
import org.eclipse.jgit.util.SystemReader;

import java.io.IOException;
import java.util.*;

public class ConfigCommand extends DocAsCodeCommand<Map<String,String>> {
    public enum Action {
        SET,
        GET,
        ADD,
        LIST
    }

    public enum Level {
        LOCAL,
        PROJECT,
        GLOBAL,
        SYSTEM
    }

    private StoredConfig getConfig(Level level) throws DocAsCodeException {
        StoredConfig config = null;
        switch (level){
            case LOCAL:
                config = getRepository().git().getConfig();
                break;
            case PROJECT:
                config = getRepository().git().getProjectConfig();
                break;
            case GLOBAL:
                config = SystemReader.getInstance().openUserConfig(null,FS.detect());
                break;
            case SYSTEM:
                config = SystemReader.getInstance().openSystemConfig(null,FS.detect());
                break;
        }
        if (null!=config) {
            try {
                config.load();
                return config;
            } catch (IOException | ConfigInvalidException e) {
                throw new DocAsCodeException("Unable to load Git configuration", e);
            }
        } else {
            throw new DocAsCodeException("Unable to get Git configuration file");
        }
    }

    private Action action = Action.SET;

    private Level level = Level.LOCAL;

    private String section = "docascode";

    private String subsection = "";

    private String name = "";

    private String value = "";

    public ConfigCommand setAction(Action action){
        this.action = action;
        return this;
    }

    public ConfigCommand setSection(String section){
        this.section = section;
        return this;
    }

    public ConfigCommand setSubsection(String subsection){
        this.subsection = subsection;
        return this;
    }

    public ConfigCommand setName(String name){
        this.name = name;
        return this;
    }

    public ConfigCommand setValue(String value){
        this.value = value;
        return this;
    }

    public ConfigCommand setLevel(Level level){
        this.level = level;
        return this;
    }

    public ConfigCommand(DocAsCodeRepository repo) {
        super(repo);
    }

    @Override
    public Map<String,String> call() throws DocAsCodeException {
        Map<String,String> result = new HashMap<>();
        switch (this.action){
            case ADD:
                add(this.level,
                        this.section,
                        this.subsection,
                        this.name,
                        this.value);
                        break;
            case GET:
                result = get(this.level,
                        this.section,
                        this.subsection,
                        this.name);
                break;
            case SET:
                set(this.level,
                        this.section,
                        this.subsection,
                        this.name,
                        this.value);
                break;
            case LIST:
                Set<String > set = new HashSet<>();
                set.add(this.section);
                result = list(this.level, set);
                break;
        }
        return result;
    }

    private void add(Level level, String section, String subsection, String name, String value) throws DocAsCodeException {
        StoredConfig config = getConfig(level);
        List<String> configList = new ArrayList<>(Arrays.asList(config.getStringList(section,subsection,name)));
        if(!configList.contains(value)){
            configList.add(value);
            config.setStringList(section,subsection,name,configList);
            try {
                config.save();
            } catch (IOException e) {
                throw new DocAsCodeException("Unable to save Git configuration",e);
            }
        }
    }

    private Map<String,String> get(Level level, String section, String subsection, String name) throws DocAsCodeException {
        StoredConfig config = getConfig(level);
        HashMap<String,String> hashMap = new HashMap<>();
        String result;
        result = config.getString(section,subsection,name);
        hashMap.put(section+"."+subsection+"."+name,result);
        return hashMap;
    }

    private void set(Level level, String section, String subsection, String name, String value) throws DocAsCodeException {
        StoredConfig config = getConfig(level);
        config.setString(section,subsection,name,value);
        try {
            config.save();
        } catch (IOException e) {
            throw new DocAsCodeException("Unable to save Git configuration",e);
        }
    }

    private Map<String,String> list(Level level, Set<String> sections) throws DocAsCodeException {
        StoredConfig config = getConfig(level);
        HashMap<String,String> hashMap = new HashMap<>();
        String result;
        for (String sec : sections){
            for (String sub : config.getSubsections(sec)){
                for (String n : config.getNames(sec,sub)){
                    result = config.getString(sec,sub,n);
                    hashMap.put(sec+"."+sub+"."+n,result);
                }
            }
        }
        return hashMap;
    }
}
