package org.docascode.ant;

import org.apache.commons.io.FilenameUtils;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.DirectoryScanner;
import org.apache.tools.ant.Task;
import org.apache.tools.ant.types.FileSet;
import org.apache.tools.ant.types.Mapper;
import org.apache.tools.ant.util.FileNameMapper;
import org.apache.tools.ant.util.IdentityMapper;
import org.docascode.api.DiffCommand;
import org.docascode.api.DocAsCode;
import org.docascode.api.core.errors.DocAsCodeException;
import org.docascode.api.event.Event;
import org.docascode.api.listener.EventListener;

import java.io.File;
import java.util.*;

public class DiffTask extends Task implements EventListener {
    private String toDir;
    public void setToDir(String toDir){
        this.toDir = toDir;
    }

    private FileSet fileSet = new FileSet();
    public void addConfiguredFileSet(FileSet fileset){
        this.fileSet = fileset;
    }

    public Mapper createMapper() {
        if (elementMapper != null) {
            throw new BuildException("Cannot define more than one mapper",
                    getLocation());
        }
        elementMapper = new Mapper(getProject());
        return elementMapper;
    }

    private Mapper elementMapper;
    public void add(FileNameMapper mapper){
        createMapper().add(mapper);
    }

    private FileNameMapper getMapper() {
        FileNameMapper mapper;
        if (elementMapper != null) {
            mapper = elementMapper.getImplementation();
        } else {
            mapper = new IdentityMapper();
        }
        return mapper;
    }

    @Override
    public void execute() {
        if (toDir == null){
            throw new BuildException("'toDir' attribute must be set.");
        }
        final DirectoryScanner scanner =
                fileSet.getDirectoryScanner(getProject());
        for (String filename : scanner.getIncludedFiles()) {
            final File fromFile =
                    new File(fileSet.getDir(getProject()), filename);
            List<String> toFileNames = new ArrayList<>();
            if (getMapper() == null){
                toFileNames.add(
                    String.format(
                        "%s/DIFF/%s.%s",
                        FilenameUtils.getPrefix(filename),
                        FilenameUtils.getBaseName(filename),
                        FilenameUtils.getExtension(filename)));
            } else {
                if ( getMapper().mapFileName(filename) != null) {
                    for (String toFileName : getMapper().mapFileName(filename)) {
                        toFileNames.add(toFileName);
                    }
                }
            }
            for (String toFileName : toFileNames){
                File toFile = new File(
                        fileSet.getDir(getProject()),
                        String.format("%s/%s",
                                toDir,
                                toFileName));
                try (DocAsCode docascode = DocAsCode.open(getProject().getBaseDir())){
                    ((DiffCommand) docascode.diff().addListener(this))
                            .setFile(fromFile)
                            .toFile(toFile)
                            .setOldRevision("LATEST")
                            .call()
                            .removeListener(this);
                } catch (DocAsCodeException e) {
                    throw new BuildException(e);
                }
            }
        }
    }

    @Override
    public void fireEvent(Event event) {
        log(event.getMessage());
    }
}
