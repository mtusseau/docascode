package org.docascode.ant.types;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.types.DataType;
import org.apache.tools.ant.types.selectors.FileSelector;
import org.docascode.api.DocAsCode;
import org.docascode.api.core.errors.DocAsCodeException;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.apache.tools.ant.Project.MSG_DEBUG;

public class ChronoSelector extends DataType implements FileSelector {
    private List<String> ids = new ArrayList<>();
    public void setIds(String ids){
        this.ids = Arrays.asList(ids.split(","));
    }

    @Override
    public boolean isSelected(File file, String s, File file1) {
        try (DocAsCode docascode = DocAsCode.open(getProject().getBaseDir())) {
            boolean selected = docascode
                        .getRepository()
                        .chrono()
                        .toFileNames(this.ids)
                        .contains(s.replace("\\","/"));
            log(String.format(
                    "'%s' selected: %s",s,selected), MSG_DEBUG);
            return selected;
            } catch (DocAsCodeException e) {
                throw new BuildException(e);
        }
    }
}
