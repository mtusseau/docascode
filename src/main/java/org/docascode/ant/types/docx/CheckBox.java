package org.docascode.ant.types.docx;

import org.apache.tools.ant.types.DataType;

public class CheckBox extends DataType {
    private String name;

    private boolean check = false;

    public void setName(final String name){
        this.name=name;
    }

    public String getName(){
        return this.name;
    }

    public void setCheck(final boolean check){
        this.check = check;
    }

    public boolean isCheck(){
        return this.check;
    }


}
