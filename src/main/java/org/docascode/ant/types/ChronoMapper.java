package org.docascode.ant.types;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.PropertyHelper;
import org.apache.tools.ant.types.DataType;
import org.apache.tools.ant.util.FileNameMapper;
import org.docascode.api.DocAsCode;
import org.docascode.api.core.errors.DocAsCodeException;

public class ChronoMapper extends DataType implements FileNameMapper {
    private String name;
    public void setName(String name){
        this.name=name;
    }

    @Override
    public void setFrom(String s) {
        throw new UnsupportedOperationException("'Chronomapper' does not support 'from' attribute");
    }

    @Override
    public void setTo(String s) {
        throw new UnsupportedOperationException("'Chronomapper' does not support 'to' attribute");
    }

    @Override
    public String[] mapFileName(String s) {
        try (DocAsCode docascode = DocAsCode.open(getProject().getBaseDir())) {
            String result = docascode
                        .getRepository()
                        .chrono()
                        .toMap(this.name)
                        .get(s.replace("\\","/"));
            PropertyHelper propertyHelper = PropertyHelper.getPropertyHelper(getProject());
            result = propertyHelper.replaceProperties(result);
                if (result != null){
                    return new String[]{result};
                } else {
                    return new String[]{};
                }
        } catch (DocAsCodeException e) {
            log(e.getMessage(), Project.MSG_WARN);
            throw new BuildException(e);
        }
    }
}
