package org.docascode.ant;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.DirectoryScanner;
import org.apache.tools.ant.Task;
import org.apache.tools.ant.types.FileSet;
import org.docascode.ant.types.docx.CheckBox;
import org.docascode.ant.types.docx.Table;
import org.docascode.api.DocAsCode;
import org.docascode.api.UpdateCommand;
import org.docascode.api.core.errors.DocAsCodeException;
import org.docascode.api.event.Event;
import org.docascode.api.listener.EventListener;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class UpdateTask extends Task implements EventListener {
    private List<File> files = new ArrayList<>();

    public void setFile(File file){
        this.files.add(file);
    }

    private FileSet fileSet;
    public void addFileSet(final FileSet fileset){
        this.fileSet = fileset;
    }

    private List<Property> properties = new ArrayList<>();

    public void addProperty (Property property){
        this.properties.add(property);
    }

    private List<Table> tables = new ArrayList<>();

    public void addTable(Table table){
        this.tables.add(table);
    }

    private Map<String,Boolean> checkBoxes = new HashMap<>();

    public void addConfiguredCheckBox(CheckBox checkBox){
        this.checkBoxes.put(checkBox.getName(),checkBox.isCheck());
    }

    @Override
    public void execute() {
        if (fileSet != null){
            final DirectoryScanner scanner =
                    fileSet.getDirectoryScanner(getProject());
            for (String filename : scanner.getIncludedFiles()) {
                this.files.add(new File(fileSet.getDir(getProject()), filename));
            }
        }
        Map<String, String> map = new HashMap<>();
        for (Property p : properties){
            map.put(p.name,p.value);
        }
        try {
            ((UpdateCommand) DocAsCode.update().addListener(this))
                    .setFiles(files)
                    .setProperties(map)
                    .setTables(tables.stream()
                            .map(Table::toCore)
                            .collect(Collectors.toList()))
                    .setCheckboxes(checkBoxes)
                    .call();
        } catch (DocAsCodeException e) {
            throw new BuildException(e);
        }
    }

    @Override
    public void fireEvent(Event event) {
        log(event.getMessage());
    }

    public static class Property{
        private String name;

        public void setName(String name){
            this.name = name;
        }

        private String value;

        public void setValue(String value){
            this.value = value;
        }
    }
}
