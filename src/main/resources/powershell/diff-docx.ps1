param (
    [string]$BaseFilePath,
    [string]$RevisedFilePath,
    [string]$Revision,
    [string]$TargetFilePath
)

$Word = $null
$Word = [System.Runtime.InteropServices.Marshal]::GetActiveObject('word.application')
if ($Word -eq $null)
{
    $Word = new-object -ComObject word.application
    $Word.visible = $false
}


Function openFile($filePath,$readOnly)
{
    $extension = [IO.Path]::GetExtension($filepath)
    Switch ($extension)
    {
        {$_ -in ".docx",".doc",".docm"} {
            $document = $Word.documents.open($filepath,$false,$readOnly)
        }
        default {
            write-host "($filepath): Wrong file extension($extension). Abort..."
            exit 1
        }
    }
    return $document
}

Function getRevision($BaseFilePath, $RevisedFilePath, $Revision, $TargetFilePath)
{
    $baseDocument = openFile $BaseFilePath $true
    $revisedDocument = openFile $RevisedFilePath $true
    try {
        $comparedDocument=$Word.CompareDocuments($baseDocument,$revisedDocument,2,1,$false, $true, $true, $true, $true, $true, $true, $true, $true, $true, $Revision);
        $comparedDocument.saveAs($TargetFilePath);
        
    }
    Finally {
        $baseDocument.close($false);
        $revisedDocument.close($false);
        $comparedDocument.close($false);
    }
 
} #end function

getRevision $BaseFilePath $RevisedFilePath $Revision $TargetFilePath

if ($null -ne $Word){
    $Word.quit()
}