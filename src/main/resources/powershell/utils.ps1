

$Word = $null
$Word = [System.Runtime.InteropServices.Marshal]::GetActiveObject('word.application')
if ($Word -eq $null)
{
    $Word = new-object -ComObject word.application
}

$Excel = $null
$Excel = [System.Runtime.InteropServices.Marshal]::GetActiveObject('excel.application')
if ($Excel -eq $null)
{
    $Excel = new-object -ComObject excel.application
}

Function openFile($filePath,$readOnly)
{
    $extension = [IO.Path]::GetExtension($filepath)
    Switch ($extension)
    {
        {$_ -in ".docx",".doc",".docm"} {
            $document = $Word.documents.open($filepath,$false,$readOnly)
        }
        {$_ -in ".xlsx",".xls",".xlsm"} {
            $document = $Excel.workbooks.open($filepath,$false,$readOnly)
        }
        default {
            write-error "($filepath): Wrong file extension($extension). Abort..." 
            exit 1
        }
    }    
    return $document
}

Function isOpen($filePath)
{
    $oFile = New-Object System.IO.FileInfo $filePath
    try {
        $oStream = $oFile.Open([System.IO.FileMode]::Open, [System.IO.FileAccess]::ReadWrite, [System.IO.FileShare]::None)
        if ($oStream) {
            $oStream.Close()
        }
        return $false
    } catch {
        # file is locked by a process.
        return $true
    }
}