package org.docascode.api;

import org.apache.commons.io.FileUtils;
import org.docascode.api.core.errors.DocAsCodeException;
import org.docascode.cli.CLIEventListener;
import org.eclipse.jgit.api.CreateBranchCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.*;
import org.eclipse.jgit.lib.*;
import org.eclipse.jgit.submodule.SubmoduleWalk;
import org.eclipse.jgit.transport.BundleWriter;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestInfo;

import java.io.*;
import java.util.Map;
import java.util.Set;

public class DocAsCodeRepositoryTestCase extends CLIEventListener {
    private final File BUNDLE = new File("target/docascode-test.bundle");

    protected Repository repository;

    private void bundleDocAsCodeTestRepository() throws IOException, GitAPIException {
        Repository mainRepo = Git.open(new File(".")).getRepository();
        Repository repo = SubmoduleWalk.getSubmoduleRepository(mainRepo, "src/test/git/docascode-test");
        Git git = Git.wrap(repo);
        for (String branch : new String[]{"develop", "support-docascode-3"}){
            git.branchCreate()
                    .setForce(true)
                    .setUpstreamMode(CreateBranchCommand.SetupUpstreamMode.SET_UPSTREAM)
                    .setName(branch)
                    .setStartPoint("origin/" + branch)
                    .call();
        }
        BundleWriter bw = new BundleWriter(repo);
        Map<AnyObjectId, Set<Ref>> refMap = repo.getAllRefsByPeeledObjectId();
        for (Map.Entry<AnyObjectId, Set<Ref>> refSet:refMap.entrySet()) {
            for (Ref ref : refSet.getValue()){
                bw.include(ref.getName(),refSet.getKey());
            }
        }
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        OutputStream outStream = new FileOutputStream(BUNDLE);
        bw.writeBundle(NullProgressMonitor.INSTANCE, out);
        out.writeTo(outStream);
    }

    @BeforeEach
    public void setUp(TestInfo testInfo) throws IOException, GitAPIException, DocAsCodeException {
        File target = new File(String.format("target/tests/%s/%s",
                getClass().getName(),
                testInfo.getDisplayName()));
        if (target.exists()) {
            FileUtils.deleteDirectory(target);
        }
        bundleDocAsCodeTestRepository();
        repository = Git.cloneRepository().setURI(BUNDLE.toURI().toString())
                .setCloneAllBranches(true)
                .setDirectory(target)
                .call()
                .getRepository();
    }

    @AfterEach
    public void cleanUp() {

    }
}
