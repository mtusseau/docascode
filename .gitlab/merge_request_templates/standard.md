## What does this MR do?

<!-- Briefly describe what this MR is about. -->

## Related issues

<!-- Link related issues below. Insert the issue link or reference after the word "Closes" if merging this should automatically close it. -->

## Author's checklist

* [ ] Docs up-to-date
* [ ] License in header for new files
* [ ] JavaDoc up-to-date
* [ ] Technical debt not increasing
* [ ] New lines are sufficiently coverered by unit tests (Aim 80%)
* [ ] No failed Unit Tests