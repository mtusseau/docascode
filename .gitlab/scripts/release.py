#! /usr/bin/env python
# -*- coding: utf-8 -*-

from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
from enum import Enum
import gitlab


class Action(Enum):
    create = 'create'
    delete = 'delete'


def get_parser():
    """Get a command line parser."""
    parser = ArgumentParser(description=__doc__,
                            formatter_class=ArgumentDefaultsHelpFormatter)

    parser.add_argument("--token",
                        dest="token",
                        help="Auth Token")
    parser.add_argument('--action',
                        type=Action,
                        choices=list(Action))
    parser.add_argument("--projectid",
                        dest="projectid",
                        type=int,
                        help="projectId of the project to handle")
    parser.add_argument("--tag",
                        dest="tag")
    return parser


def create(token, projectid, tag):
    gl = gitlab.Gitlab('https://gitlab.com', private_token=token)
    project = gl.projects.get(projectid)
    release = project.releases.create({'name': project.name+' ' + tag,
                                       'tag_name': tag,
                                       'description': project.name+' version ' + tag,
                                       'assets':
                                           {'links':
                                               [{'name': 'Windows Installer', 'url': project.web_url+'/-/jobs/artifacts/' + tag +'/download?job=package:win'}]}})
    print(release)


def delete(token, projectid, tag):
    gl = gitlab.Gitlab('https://gitlab.com', private_token=token)
    project = gl.projects.get(projectid)
    release=project.releases.delete(tag)
    print(release)


if __name__ == '__main__':
    parser = get_parser()
    args = parser.parse_args()
    if args.action == Action.create:
        create(args.token,
            args.projectid,
            args.tag)
    elif args.action == Action.delete:
        delete(args.token,
            args.projectid,
            args.tag)
